import express from 'express';
import template from './config/template';
import staticFiles from './config/static';
import fs from 'fs';
import path from 'path';

const app = express();
template(app);
staticFiles(app);


const readFileAsync = async (path) => {
	return new Promise((resolve, reject) => {
		fs.readFile(path, {encoding: 'utf-8'}, (err, data) => {
			if (err) {
				reject(error);
			} else {
				resolve(data);
			}
		});
	});
}

const getDepartments = (cities) => {
	const cityCodes = [];
	const provinceCodes = [];
	const distritCodes = [];

	const departments = [];
	const provinces = [];
	const distrits = [];
	cities.forEach(city => {
		if (!cityCodes.includes(city.department[0])) {
			cityCodes.push(city.department[0]); // push code
			departments.push({
				code: city.department[0],
				name: city.department[2] ? `${city.department[1]} ${city.department[2]}` : city.department[1],
			});
		}
		if (!provinceCodes.includes(city.province[0]) && city.province[0] !== '') {
			provinceCodes.push(city.province[0]); // push code
			provinces.push({
				code: city.province[0],
				name: city.province[2] ? `${city.province[1]} ${city.province[2]}` : city.province[1],
				departmentCode: city.department[0],
				deparmentName: city.department[2] ? `${city.department[1]} ${city.department[2]}` : city.department[1],
			});
		}

		if (!distritCodes.includes(city.distrit[0]) && city.distrit[0] !== '') {
			distritCodes.push(city.distrit[0]); // push code
			distrits.push({
				code: city.distrit[0],
				name: city.distrit[2] ? `${city.distrit[1]} ${city.distrit[2]}` : city.distrit[1],
				provinceCode: city.province[0],
				provinceName: city.province[2] ? `${city.province[1]} ${city.province[2]}` : city.province[1],
			});
		}
	});
	return [departments, provinces, distrits];
};

app.get('/', async (req, res) => {
	const filePath = path.join(__dirname, 'client', 'files', 'ubigeos.txt');
	let dataFile = await readFileAsync(filePath);
	dataFile = dataFile
							.split('/')
							.join()
							.split('\n');
	const cities = dataFile.map(data => {
		const line = data.split(',');
		const placeAndCode = line[0].split(' ')
		const city = { department: line[0].trim().split(' '), province: line[1].trim().split(' '), distrit: line[2].trim().split(' ') };
		return city;
	});
	const finalData = getDepartments(cities);
	res.locals.departments = finalData[0];
	res.locals.provinces = finalData[1];
	res.locals.distrits = finalData[2]
	return res.render('home.html');
});

app.listen(3000);
